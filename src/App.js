import { Box, Footer, Grommet, Heading, Image } from 'grommet';
import { grommet } from 'grommet/themes';
import { deepMerge } from 'grommet/utils';
import React from 'react';
import THEME_PROPS from './theme.js';
const logoWhite = require('./whitelogo.png');
const pythonLogo = require('./python-xxl.png');
const reactLogo = require('./react.png');
const goLangLogo = require('./golang.png');
const flutterLogo = require('./flutter.png');

const customTheme = deepMerge(grommet, THEME_PROPS);

function App() {
  // const d = new Date();
  // const year = d.getFullYear();
  return (
    <Grommet full theme={customTheme}>
      <Box
        background="#28282B"
        align="center"
        justify="center"
        fill
        gap="medium"
        style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Box align="center">
          <Box
            align="center"
            direction="row"
            style={
              {
                // position: 'absolute',
                // top: '50px',
              }
            }
          >
            <Image src={logoWhite} width="60px" height="60px" />
            <Heading level="1" style={{ margin: 0 }}>
              Qolphin
            </Heading>
          </Box>
          <Heading level="3">Software Innovation and Research Lab</Heading>
        </Box>

        <Box align="center">
          <Box
            gap="small"
            direction="row"
            margin={{ vertical: 'medium' }}
            align="center"
            wrap
            justify="center"
          >
            <Image src={pythonLogo} width="40px" height="40px" title="Python" />
            <Image
              src={reactLogo}
              width="45px"
              height="40px"
              title="ReactJs"
              style={{ marginLeft: 20 }}
            />
            <Image src={goLangLogo} width="90px" height="90px" title="Golang" />
            <Image
              src={flutterLogo}
              width="125px"
              height="40px"
              title="Flutter"
            />
          </Box>
        </Box>
      </Box>
      <Footer
        pad="medium"
        background="#28282B"
        style={{
          position: 'block',
          width: '100%',
          bottom: 0,
          margin: 'auto',
        }}
      >
        {/* <Text>Copyright {year}</Text> */}
        {/* <Anchor
					label="A Product of Saturnq Solutions"
					href="https://www.saturnq.com"
					target="_blank"
					color="white"
				/> */}
      </Footer>
    </Grommet>
  );
}

export default App;

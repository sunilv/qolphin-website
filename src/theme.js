import { css } from "styled-components";

const APP_COLORS = {
	brand: "#562e91"
};

const THEME_PROPS = {
	global: {
		colors: APP_COLORS,
		font: {
			family: `'Nunito', sans-serif;`,
			size: "15px"
		},
		edgeSize: {
			small: "10px"
		},
		elevation: {
			light: {
				small: "0px 1px 5px rgba(0, 0, 0, 0.50)",
				medium: "0px 3px 8px rgba(0, 0, 0, 0.50)"
			}
		}
	},
	button: {
		border: {
			radius: "1px"
		}
	},
	paragraph: {
		extend: ({ theme }) => css`
			font-size: 16px;
		`
	},

	text: {
		medium: {
			size: "15px"
		}
	},
	tab: {
		active: {
			background: "primary",
			color: "white"
		},
		background: "white",
		border: undefined,
		color: "brand",
		hover: {
			background: "primary",
			color: "white"
		},
		margin: undefined,
		pad: {
			vertical: "xsmall",
			horizontal: "small"
		},
		extend: ({ theme }) => css`
			border-radius: 5px;
		`
	},
	tabs: {
		background: "background",

		gap: "medium",
		margin: {
			medium: "small"
		},
		header: {
			background: "background",
			extend: ({ theme }) => css`
				padding: ${theme.global.edgeSize.small};
			`
		},
		panel: {
			extend: ({ theme }) => css`
				padding: 0;
			`
		}
	}
};
export default THEME_PROPS;
